// requires...
const fs = require('fs');
const path = require('path');
// constants...
const FILE_PATH = './files/';
const FILE_EXTENSIONS = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];


function createFile (req, res, next) {
  // Your code to create the file.
  const { filename, content } = req.body;
  const filePath = FILE_PATH + filename;
  console.log(filePath);
  if (!FILE_EXTENSIONS.includes(path.extname(filePath))) {
    return next({
      status: 400,
      message: 'Application supports: .log, .txt, .json, .yaml, .xml, .js',
    });
  }

  if (!content) {
    return next({
      status: 400,
      message: 'Please specify "content" parameter',
    });
  }

  if (fs.existsSync(filePath)) {
    return next({
      status: 400,
      message: "FIle already exist",
    });
  }

  fs.writeFile(filePath, content, (err) => {
    if (err) {
      return next({
        status: 400,
        message: err.message,
      })
    } else {
      res.status(200).send({ message: 'File created successfully' });
    }
  })
}

function getFiles (req, res, next) {
  // Your code to get all files.

  fs.readdir(FILE_PATH, (err, files) => {
    if (err) {
      return next({
        status: 400,
        message: 'Client error',
      })
    }

    res.status(200).send({
      message: 'Success',
      files: files,
    });
  });
}

const getFile = (req, res, next) => {
  // Your code to get all files.
  const { filename } = req.params;
  const filePath = FILE_PATH + filename;
  if (!fs.existsSync(filePath)) {
    return next({
      status: 400,
      message: `No file with filename ${filename} found`,
    })
  }

  res.status(200).send({
    message: 'Success',
    filename: filename,
    content: fs.readFileSync(filePath).toString(),
    extension: path.extname(filePath).substr(1),
    uploadedDate: fs.statSync(filePath).birthtime,
  });
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
